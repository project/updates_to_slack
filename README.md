# Updates to slack

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration

INTRODUCTION
------------

This module sent core and module updates to your slack channel

REQUIREMENTS
------------

 * This module requires slack api key.

INSTALLATION
------------

The installation of this module is like other Drupal modules.

 1. Copy/upload the updates_to_slack module to the modules directory.

 2. Enable the 'updates_to_slack' module and desired sub-modules in 'Extend'.
   (/admin/modules)

CONFIGURATION
-------------

 * Configure your slack cre with product has taxonomy term
 * Go to Updates to Slack Settings.
	> Note: Configuration -> System -> Updates to Slack
