<?php

namespace Drupal\updates_to_slack\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\RequestException;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\update\UpdateManagerInterface;
use Drupal\update\UpdateFetcherInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class utitlies.
 */
class UpdatesToSlackController extends ControllerBase {

  use StringTranslationTrait;

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $config;

  /**
   * Http client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  private $httpClient;

  /**
   * Logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  private $logger;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a Slack object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   Module configuration.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   HTTP Client.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   Logger.
   */
  public function __construct(ConfigFactoryInterface $config, ClientInterface $http_client, LoggerChannelFactoryInterface $logger) {
    $this->config = $config;
    $this->httpClient = $http_client;
    $this->logger = $logger;

  }

  /**
   * {@inheritdoc}
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The Drupal service container.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('http_client'),
      $container->get('logger.factory')
    );
  }

  /**
   * Prepare message meta fields for Slack.
   *
   * @param string $webhook_url
   *   Webhook for Slack basic functions.
   * @param string $channel
   *   The channel in the Slack service to send messages.
   * @param string $username
   *   The bot name displayed in the channel.
   *
   * @return array
   *   Config array.
   */
  protected function prepareMessage($webhook_url = '', $channel = '', $username = '') {
    $config = $this->config->get('updates_to_slack.settings');
    $message_options = [];

    if (!empty($channel)) {
      $message_options['channel'] = $channel;
    }
    elseif (!empty($config->get('slack_channel'))) {
      $message_options['channel'] = $config->get('slack_channel');
    }

    if (!empty($username)) {
      $message_options['username'] = $username;
    }
    elseif (!empty($config->get('slack_username'))) {
      $message_options['username'] = $config->get('slack_username');
    }
    $icon_type = $config->get('slack_icon_type');

    if ($icon_type == 'emoji') {
      $message_options['icon_emoji'] = $config->get('slack_icon_emoji');
    }
    elseif ($icon_type == 'image') {
      $message_options['icon_url'] = $config->get('slack_icon_url');
    }
    // $message_options['as_user'] = TRUE;
    return [
      'webhook_url' => $webhook_url,
      'message_options' => $message_options,
    ];
  }

  /**
   * Send message to the Slack with more options.
   *
   * @param string $webhook_url
   *   Webhook for Slack basic functions.
   * @param string $message
   *   The message sent to the channel.
   * @param array $message_options
   *   An associative array, it can contain:
   *     - channel: The channel in the Slack service to send messages;
   *     - username: The bot name displayed in the channel;
   *     - icon_emoji: The bot icon displayed in the channel;
   *     - icon_url: The bot icon displayed in the channel.
   *
   * @return \Psr\Http\Message\ResponseInterface|bool
   *   Can contain:
   *                          success    fail         fail
   *     - data:                ok       No hooks     Invalid channel specified
   *     - status message:      OK       Not found    Server Error
   *     - code:                200      404          500
   *     - error:               -        Not found    Server Error
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  protected function sendRequest($webhook_url, $message, array $message_options = []) {
    $headers = [
      'Content-Type' => 'application/x-www-form-urlencoded',
    ];
    $message_options['text'] = $this->processMessage($message);
    $sending_data = 'payload=' . urlencode(json_encode($message_options));
    $logger = $this->logger->get('updates_to_slack');
    try {
      $response = $this->httpClient->request('POST', $webhook_url, ['headers' => $headers, 'body' => $sending_data]);
      $logger->info('Message was successfully sent!');
      return $response;
    }
    catch (ServerException $e) {
      $logger->error('Server error! It may appear if you try to use unexisting chatroom.');
      return FALSE;
    }
    catch (RequestException $e) {
      $logger->error('Request error! It may appear if you entered the invalid Webhook value.');
      return FALSE;
    }
  }

  /**
   * Replaces links with slack friendly tags. Strips all other html.
   *
   * @param string $message
   *   The message sent to the channel.
   *
   * @return string
   *   Replaces links with slack friendly tags. Strips all other html.
   */
  protected function processMessage($message) {
    $regexp = "<a\s[^>]*href=(\"??)([^\" >]*?)\\1[^>]*>(.*)<\/a>";

    if (preg_match_all("/$regexp/siU", $message, $matches, PREG_SET_ORDER)) {
      $i = 1;
      $links = [];
      foreach ($matches as $match) {
        $new_link = "<$match[2] | $match[3]>";
        $links['link-' . $i] = $new_link;
        $message = str_replace($match[0], 'link-' . $i, $message);
        $i++;
      }
      $message = strip_tags($message);
      foreach ($links as $id => $link) {
        $message = str_replace($id, $link, $message);
      }
    }
    return $message;
  }

  /**
   * Send message to the Slack.
   *
   * @param string $message
   *   The message sent to the channel.
   * @param string $channel
   *   The channel in the Slack service to send messages.
   * @param string $username
   *   The bot name displayed in the channel.
   *
   * @return bool|object
   *   Slack response.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function sendAttachmentMessage($attachment = [], $webhook_url = '', $channel = '', $username = '') {

    $config = $this->config->get('updates_to_slack.settings');
    $webhook_url = $config->get('slack_webhook_url');

    if (empty($webhook_url)) {
      $this->messenger->addError($this->t('You need to enter a webhook!'));
      return FALSE;
    }

    $config = $this->prepareMessage($webhook_url, $channel, $username);
    $messageOptions = $config['message_options'];
    $this->logger->get('slack')
      ->info('Sending Slack message to @channel channel as "@username"', [
        '@channel' => $messageOptions['channel'],
        '@username' => $messageOptions['username'],
      ]);

    $attachment['color'] = '#f00';
    $messageOptions['attachments'][] = $attachment;
    $result = $this->sendAttachmentRequest($config['webhook_url'], $messageOptions);
    return $result;
  }

  /**
   * Send message to the Slack with more options.
   *
   * @param string $webhook_url
   *   Webhook for Slack basic functions.
   * @param array $message_options
   *   An associative array, it can contain:
   *     - channel: The channel in the Slack service to send messages;
   *     - username: The bot name displayed in the channel;
   *     - icon_emoji: The bot icon displayed in the channel;
   *     - icon_url: The bot icon displayed in the channel.
   *
   * @return \Psr\Http\Message\ResponseInterface|bool
   *   Can contain:
   *                          success    fail         fail
   *     - data:                ok       No hooks     Invalid channel specified
   *     - status message:      OK       Not found    Server Error
   *     - code:                200      404          500
   *     - error:               -        Not found    Server Error
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  protected function sendAttachmentRequest($webhook_url, array $message_options = []) {
    $headers = [
      'Content-Type' => 'application/json',
    ];
    $sending_data = json_encode($message_options);

    $logger = $this->logger->get('updates_to_slack');
    try {
      $response = $this->httpClient->request('POST', $webhook_url, ['headers' => $headers, 'body' => $sending_data]);
      $logger->info('Message was successfully sent!');
      $content = $response->getBody();
      return json_decode($content, TRUE);
    }
    catch (ServerException $e) {
      $logger->error('Server error! It may appear if you try to use unexisting chatroom.' . $e->getMessage());
      return FALSE;
    }
    catch (RequestException $e) {
      $logger->error('Request error! It may appear if you entered the invalid Webhook value.' . $e->getMessage());
      return FALSE;
    }
  }

  /**
   *
   */
  public function getUpdates() {
    $availableUpdates = update_get_available(TRUE);
    $status = [
      -4 => 'Fetch Pending',
      -3 => 'Not Fetched',
      -2 => 'Unknown',
      -1 => 'Not Checked',
      1 => 'Security Update',
      2 => 'No longer available',
      3 => 'Not supported',
      4 => 'Minor Update modules',
      5 => 'uptodate',
    ];
    $statusCircle = [
      -4 => '⚫',
      -3 => '⚫',
      -2 => '⚫',
      -1 => '⚫',
      1 => '🔴',
      2 => '⚫',
      3 => '⚫',
      4 => '🟠',
      5 => '🟢',
    ];

    \Drupal::moduleHandler()->loadInclude('update', 'inc', 'update.compare');
    $project_data = update_calculate_project_data($availableUpdates);
    $needUpdates = [];
    foreach ($project_data as $name => $project) {
      extract($project);
      if ($status === UpdateFetcherInterface::NOT_FETCHED) {
        $fetch_failed = TRUE;
      }

      // Filter out projects which are up to date already.
      if ($status == UpdateManagerInterface::CURRENT) {
        continue;
      }
      if(isset($title) && isset($latest_version)) {
        $needUpdates[$project_type][$status][$name] = [
          'title' => $title,
          'link' => $link,
          'status_emoji' => $statusCircle[$status],
          'existing_version' => $existing_version,
          'latest_version' => $latest_version,
        ]; 
      }
    }
    return $needUpdates;
  }

  /**
   *
   */
  public function prepareSlackUpdateData() {
    $needUpdates = $this->getUpdates();
    $header = [];
    $siteName = \Drupal::config('system.site')->get('name');
    $header[] = [
      "type" => "header",
      "text" => [
        "type" => "plain_text",
        "text" => $siteName . " Drupal Core & Module Updates",
        "emoji" => TRUE,
      ],
    ];
    $header[] = [
      "type" => "divider",
    ];
    $blocks = [];
    foreach ($needUpdates as $projectTypeId => $updates) {
      $text = '';
      foreach ($updates as $updateTypeId => $udpateProjects) {
        foreach ($udpateProjects as $projectName => $projectData) {
          extract($projectData);
          $charLen = strlen($title);
          $maxchar = 40;
          $remainChar = 40 - $charLen;
          $tab_chars = '';
          for ($i=0; $i < $remainChar ; $i++) { 
            $tab_chars .= ' ';
          }
          // $text .= $status_emoji . ' ' . ucfirst($title) . $tab_chars . ' : _*( ' . $existing_version . ' ~> ' . $latest_version . " )*_ \n";
          $text .= $status_emoji . ' ' . ucfirst($title) . $tab_chars . ' : ( ' . $existing_version . ' ~> ' . $latest_version . " ) \n";
        }
      }
      if (!empty($text)) {
        $blocks[] = [
          "type" => "section",
          "text" => [
            "type" => "mrkdwn",
            "text" => '*' . ucfirst($projectTypeId) . ' Updates*',
          ],
        ];
        $blocks[] = [
          "type" => "section",
          "text" => [
            "type" => "mrkdwn",
            "text" => '```' . $text . '```',
          ],
        ];
      }
    }
    if (count($blocks) > 0) {
      $header[] = [
        "type" => "section",
        "text" => [
          "type" => "mrkdwn",
          "text" => '*🔴 - Security Update | 🟠 - Minor Update | ⚫ - Not available*'
        ],
      ];
      $blocks = array_merge($header, $blocks);
      return ['blocks' => $blocks];
    }
    else {
      $header[] = [
        "type" => "section",
        "text" => [
          "type" => "mrkdwn",
          "text" => '*There are no updates*',
        ],
      ];
      return ['blocks' => $header];
    }
  }

}
